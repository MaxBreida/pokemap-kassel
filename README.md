## Getting Started

* Clone this repository.
* Run `npm install` from the project root.
* Install the ionic CLI (`npm install -g ionic@beta`)
* Run `ionic serve --lab` in a terminal from the project root.
* Profit